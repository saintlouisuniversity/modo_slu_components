<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

/**
 * @ingroup DataModel
 *
 * @brief A subclass of KGOItemsDataModel to handle data from multiple feeds.
 *
 */

class SaintLouisAggregateDataModel extends KGOAggregateDataModel
{
    public function getItems(&$response = null) {
        $response = KGODataResponse::factory('KGOAggregateDataResponse', array());
        $items = array();
        foreach ($this->getFeeds() as $feed) {
            $origFeedOptions = $feed->getOptions();

            $this->applyAggregateFeedOptions($feed);
            $items = array_merge($items, $feed->getItems($_response));
            $response->addResponse($feed->getId(), $_response);

            $this->restoreAggregateFeedOptions($feed, $origFeedOptions);
        }

        foreach ($items as $item) {
            $item->setBookmarkDataModelId($this->id);
        }
        //usort($items, static::$defaultSortCallback);

        $this->setTotalItems(count($items));
        $items = $this->limitItems($items, $this->getStart(), $this->getLimit());

        return $items;
    }
}