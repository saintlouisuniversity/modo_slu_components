<?php
/*
* Copyright © 2010 - 2013 Modo Labs Inc. All rights reserved.
*
* The license governing the contents of this file is located in the LICENSE
* file located at the root directory of this distribution. If the LICENSE file
* is missing, please contact sales@modolabs.com.
*
*/
/**
* @ingroup DataParser
*
* @brief Does not display an alert feed whose title matches clearMessage
*
*/
class ClearRssAlertDataParser extends KGOAlertRSSDataParser
{
    protected function parseRSSItem($itemData) {
        $clearMessage = "All clear*";
        $item = parent::parseRSSItem($itemData);
        
        if (strcasecmp($item->getDescription(), $clearMessage)==0) {
            return null;
        }
        return $item;
    }
}